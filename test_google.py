import unittest
from selenium import webdriver
from pyvirtualdisplay import Display

class TestSelenium(unittest.TestCase):
    def test_browse_google(self):
        driver = webdriver.Chrome()
        driver.get('https://www.google.com')
        print driver.title
        assert driver.title == "Google"
    def test_browse_firefox(self):
        display = Display(visible=0, size=(800, 600))
        display.start()
        driver = webdriver.Firefox()
        driver.get('https://www.google.com')
        print driver.title
        assert driver.title == "Google"

if __name__=='__main__':
    unittest.main()