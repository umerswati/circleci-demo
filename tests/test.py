import unittest
from selenium import webdriver

class TestSelenium(unittest.TestCase):
    def test_browse_google(self):
        driver = webdriver.Chrome()
        driver.get('https://www.google.com')
        print driver.title
        assert driver.title == "Google"

if __name__=='__main__':
    unittest.main()